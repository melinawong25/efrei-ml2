# EFREI - Machine Learning II

Dépôt git de la promotion M1APP-BDML de l'Efrei pour les interventions :

- 19 et 20 janvier 2023 : séries temporelles
- 20 janvier 2023 : packaging d'un modèle ML dans un web-service (application web avec Flask) ?

Intervenant : Louis KUHN
